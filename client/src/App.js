import React, {Component} from 'react';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';

import BookList from './components/Booklist';
import AddBook from './components/AddBook';

// Apollo Client Setup
const client = new ApolloClient({uri: 'http://localhost:4000/graphql'});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        {/*dynamically output data in react component*/}
        <div id="main">
          <h1>Book List Project</h1>
          <BookList/>
          <AddBook/>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
